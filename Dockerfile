FROM node:21.5.0-alpine

WORKDIR /app

COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install

COPY bin bin
COPY index.js index.js

CMD ["npm", "run", "start"]
