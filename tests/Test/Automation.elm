module Test.Automation exposing (..)

import Automation
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Integration.Zigbee2Mqtt.Z2MDevice exposing (Z2MDevice)
import Test exposing (..)
import ZigbeeDevice


suite : Test
suite =
    let
        dummyZigbeeDevice =
            Z2MDevice "dummy" ZigbeeDevice.IkeaTradfriBulbE27Cws806lm
    in
    describe "Automation.allConditionsMet"
        [ test "returns False if no conditions provided" <|
            \_ ->
                let
                    actual =
                        Automation.allConditionsMet dummyZigbeeDevice []
                in
                Expect.equal actual False
        , test "Returns false if any conditions is false" <|
            \_ ->
                let
                    actual =
                        Automation.allConditionsMet dummyZigbeeDevice [ False, True ]
                in
                Expect.equal actual False
        , test "returns true if all conditions is true" <|
            \_ ->
                let
                    actual =
                        Automation.allConditionsMet dummyZigbeeDevice [ True, True ]
                in
                Expect.equal actual True
        ]
