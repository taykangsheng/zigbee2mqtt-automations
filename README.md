# Zigbee2MQTT Automations

My own Zigbee2MQTT Automation pipeline in Elm. Main file is currently `Automations.elm`.

```bash
# Start automation server.
#   Pre-requisite: npm install -g nodemon
npm run server

# Compile elm application
#   Pre-requisite: npm install -g elm-live
npm run build

# Build docker image
#   Pre-requiresite: Docker
npm run build-image
```

## Notes

Use this command to start docker container in synology. Remember to use the correct image.
```
docker run --name zigbee2mqtt_automations_v1.0.3 -v /volume1/docker/zigbee2mqtt/zigbee2mqtt-data:/app/zigbee2mqtt-data registry.gitlab.com/taykangsheng/zigbee2mqtt-automations:v1.0.3
```
