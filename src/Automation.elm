module Automation exposing (..)

import Integration.Zigbee2Mqtt.Z2MDevice as Z2MDevice
import Json.Encode as E
import Logger
import MQTT
import ZigbeeDevice exposing (ZigbeeDevice(..))
import ZigbeeDevice.IkeaTradfriBulbE27Cws806lm


{-| List of generic actions
-}
type Action
    = On
    | Off


{-| List of generic conditions
-}
type alias Condition =
    Bool


{-| Decribes an action to be done
-}
type alias XAction =
    { targetDeviceName : String
    , action : Action
    }


type alias XCondition =
    { targetDeviceName : String
    , condition : Condition
    }


{-| Describes a single automation
-}
type alias Automation =
    { triggerDeviceName : String
    , conditions : List Condition
    , actions : List XAction
    }


type alias AutomationError =
    { deviceName : String
    , errorMsg : String
    }


{-| Execute relevant rules.

    TODO: Can be improve?

-}
exec : List Z2MDevice.Z2MDevice -> Automation -> Result AutomationError (Cmd msg)
exec devices automation =
    case Z2MDevice.findRegisteredDevice automation.triggerDeviceName devices of
        Just triggerDevice ->
            -- Trigger device found
            if allConditionsMet triggerDevice automation.conditions then
                -- All conditions met, executing automation actions
                automation.actions
                    |> List.map
                        (\xAction ->
                            case Z2MDevice.findRegisteredDevice xAction.targetDeviceName devices of
                                Just xDevice ->
                                    -- Target Device found, translating to commands
                                    translateAction xDevice xAction.action
                                        |> Result.map
                                            (\a ->
                                                Cmd.batch
                                                    [ a
                                                    , Logger.send
                                                        { level = Logger.Info
                                                        , message =
                                                            String.concat [ "[", automation.triggerDeviceName, "] -> ", "[", xAction.targetDeviceName, "][", actionToString xAction.action, "]" ]
                                                        , namespace = "Automation"
                                                        , metaData = Nothing
                                                        }
                                                    ]
                                            )

                                Nothing ->
                                    -- Target Device not found, skipping automation with error
                                    Err
                                        { deviceName = xAction.targetDeviceName
                                        , errorMsg = "target device not found"
                                        }
                        )
                    |> List.map
                        (\result ->
                            case result of
                                Ok command ->
                                    -- action is supported by device, execute commands
                                    command

                                Err error ->
                                    -- action is not supported by device, no-op commands
                                    logAutomationError error
                        )
                    |> Cmd.batch
                    |> Ok

            else
                -- Conditions not met, skipping automation
                Ok Cmd.none

        Nothing ->
            -- Trigger Device not found, skipping automation with error
            Err
                { deviceName = automation.triggerDeviceName
                , errorMsg = "Trigger Device not found"
                }


{-| Loop through all automations and execute all relevant rules

    TODO: The name is confusing! find a better name.

-}
execAll : Z2MDevice.Z2MDevice -> List Z2MDevice.Z2MDevice -> List Automation -> Result AutomationError (Cmd msg)
execAll triggerDevice registeredDevices automations =
    automations
        |> List.filter (\automation -> automation.triggerDeviceName == triggerDevice.friendlyName)
        |> List.map (\automation -> exec registeredDevices automation)
        |> List.foldl
            (\result foldedValue ->
                case result of
                    Ok command ->
                        command :: foldedValue

                    Err error ->
                        logAutomationError error :: foldedValue
            )
            []
        |> Cmd.batch
        |> Ok


{-| Check if conditions is met for device.
-}
allConditionsMet : Z2MDevice.Z2MDevice -> List Bool -> Bool
allConditionsMet _ conditions =
    if List.isEmpty conditions then
        False

    else
        List.foldl
            (\item foldedValue -> item == True && foldedValue == True)
            True
            conditions


{-| Mapping between Device and actions. Can consider pushing this mapping to each device's file.
-}
translateAction : Z2MDevice.Z2MDevice -> Action -> Result AutomationError (Cmd msg)
translateAction device action =
    case ( device.zigbeeDevice, action ) of
        ( ZigbeeDevice.IkeaTradfriBulbE27Cws806lm, On ) ->
            Z2MDevice.createMqttEvent (ZigbeeDevice.IkeaTradfriBulbE27Cws806lm.toggle True) device
                |> MQTT.sendMQTTEvent
                |> Ok

        ( ZigbeeDevice.IkeaTradfriBulbE27Cws806lm, Off ) ->
            Z2MDevice.createMqttEvent (ZigbeeDevice.IkeaTradfriBulbE27Cws806lm.toggle False) device
                |> MQTT.sendMQTTEvent
                |> Ok

        ( _, _ ) ->
            Err
                { deviceName = device.friendlyName
                , errorMsg = "Action" ++ actionToString action ++ " is not supported by device"
                }


actionToString : Action -> String
actionToString action =
    case action of
        On ->
            "On"

        Off ->
            "Off"


logAutomationError : AutomationError -> Cmd msg
logAutomationError error =
    Logger.send
        { level = Logger.Error
        , namespace = "Automation"
        , message = error.errorMsg
        , metaData =
            Just
                (E.object
                    [ ( "deviceName", E.string error.deviceName )
                    ]
                )
        }
