module Main exposing (..)

import Automation
import Config exposing (automationRules)
import Integration.Zigbee2Mqtt.Main exposing (registerZ2MDevices)
import Integration.Zigbee2Mqtt.Z2MDevice as Z2MDevice
import Integration.Zigbee2Mqtt.Z2MDeviceRegistrationInput exposing (Z2MDeviceRegistrationInput)
import Json.Encode as E
import Logger
import MQTT


type alias AppConfig =
    { mqttAddress : String
    }


type alias AppModel =
    { mqtt : MQTT.State
    , zigbee2mqtt :
        { registeredDevices : List Z2MDevice.Z2MDevice
        }
    }


type Message
    = RegisterMQTTConnection MQTT.ConnectionResponse
    | RegisterZ2MDevices (List Z2MDeviceRegistrationInput)
    | SubscribedToMqttTopic String
    | GotMQTTEvent MQTT.Event


main : Program AppConfig AppModel Message
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


init : AppConfig -> ( AppModel, Cmd Message )
init appConfig =
    ( { mqtt = MQTT.PendingConnection
      , zigbee2mqtt = { registeredDevices = [] }
      }
      -- When app is ready, initiate connection to MQTT broker
    , Cmd.batch
        [ MQTT.connectToMQTT appConfig.mqttAddress
        , sendLog "Initializing Automation App" Nothing
        ]
    )


update : Message -> AppModel -> ( AppModel, Cmd Message )
update msg model =
    case msg of
        RegisterMQTTConnection connectionResponse ->
            case connectionResponse.error of
                Just e ->
                    -- TODO: Might want to attempt to connect again periodically
                    ( { model | mqtt = MQTT.Error e }, Cmd.none )

                Nothing ->
                    -- Initialize with no devices
                    ( { model
                        | mqtt =
                            MQTT.Connected
                                { address = connectionResponse.address
                                , subscribedTopics = []
                                }
                      }
                    , sendLog ("Connected to MQTT: " ++ connectionResponse.address) Nothing
                    )

        RegisterZ2MDevices devices ->
            case model.mqtt of
                MQTT.Connected _ ->
                    let
                        z2mDevices : List Z2MDevice.Z2MDevice
                        z2mDevices =
                            List.filterMap Z2MDevice.lookupDevice devices
                    in
                    ( { model | zigbee2mqtt = { registeredDevices = z2mDevices } }
                    , z2mDevices
                        |> List.map
                            (\device ->
                                [ sendLog ("Registered Zigbee2Mqtt device: " ++ device.friendlyName) Nothing
                                , MQTT.subscribeTopic (Z2MDevice.getZ2MTopicName device)
                                ]
                            )
                        |> List.concat
                        |> Cmd.batch
                    )

                MQTT.PendingConnection ->
                    ( model
                    , sendLog "Pending connection to MQTT" Nothing
                    )

                MQTT.Error error ->
                    ( model
                    , sendErrorLog ("Error connecting to MQTT: " ++ error) Nothing
                    )

        SubscribedToMqttTopic mqttTopic ->
            case model.mqtt of
                MQTT.Connected mqttConfig ->
                    ( { model
                        | mqtt =
                            mqttConfig
                                |> MQTT.mapConfig (\config -> { config | subscribedTopics = mqttTopic :: config.subscribedTopics })
                                |> MQTT.Connected
                      }
                    , sendLog ("Subscribed to MQTT topic: " ++ mqttTopic) Nothing
                    )

                _ ->
                    ( model, Cmd.none )

        GotMQTTEvent mqttEvent ->
            case model.mqtt of
                MQTT.Connected _ ->
                    ( model
                    , Cmd.batch
                        [ -- find registered devices
                          Z2MDevice.findRegisteredDeviceFromMqttTopic mqttEvent.topic model.zigbee2mqtt.registeredDevices
                            -- loop through all automation rules
                            |> Maybe.map (\device -> Automation.execAll device model.zigbee2mqtt.registeredDevices (automationRules mqttEvent))
                            -- handle automation failure if any
                            |> Maybe.map (\result -> Result.withDefault (sendLog "Automation failed" Nothing) result)
                            -- handle fail to find device from mqtt topic
                            |> Maybe.withDefault (sendLog "Device not found" Nothing)
                        , sendLog "GotMQTTEvent" (Just (MQTT.eventToJSON mqttEvent))
                        ]
                    )

                MQTT.PendingConnection ->
                    ( model
                    , sendLog "Pending connection to MQTT" Nothing
                    )

                MQTT.Error error ->
                    ( model
                    , sendLog ("Error connecting to MQTT: " ++ error) Nothing
                    )


subscriptions : AppModel -> Sub Message
subscriptions _ =
    Sub.batch
        [ MQTT.gotMQTTConnection RegisterMQTTConnection
        , MQTT.gotMQTTEvent GotMQTTEvent
        , MQTT.gotSubscribedTopic SubscribedToMqttTopic
        , registerZ2MDevices RegisterZ2MDevices
        ]


{-| Send INFO log
-}
sendLog : String -> Maybe E.Value -> Cmd msg
sendLog message metaData =
    Logger.send
        { level = Logger.Info
        , namespace = "Main"
        , message = message
        , metaData = metaData
        }


{-| Send ERROR log
-}
sendErrorLog : String -> Maybe E.Value -> Cmd msg
sendErrorLog message metaData =
    Logger.send
        { level = Logger.Error
        , namespace = "Main"
        , message = message
        , metaData = metaData
        }
