port module Logger exposing (Level(..), LogMessage, send)

import Json.Encode as E


type Level
    = Info
    | Error


type alias LogMessage =
    { level : Level
    , namespace : String
    , message : String
    , metaData : Maybe E.Value
    }


send : LogMessage -> Cmd msg
send logMessage =
    log
        (E.object
            [ ( "level", E.string (logLevelToString logMessage.level) )
            , ( logMessage.namespace
              , E.object
                    [ ( "message", E.string logMessage.message )
                    , ( "metaData", Maybe.withDefault E.null logMessage.metaData )
                    ]
              )
            ]
        )


logLevelToString : Level -> String
logLevelToString level =
    case level of
        Info ->
            "INFO"

        Error ->
            "ERROR"


port log : E.Value -> Cmd msg
