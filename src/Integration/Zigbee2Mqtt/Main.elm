port module Integration.Zigbee2Mqtt.Main exposing (..)

import Integration.Zigbee2Mqtt.Z2MDeviceRegistrationInput exposing (Z2MDeviceRegistrationInput)
import ZigbeeDevice exposing (ZigbeeDevice(..))


{-| Devices to be registered in app
-}
port registerZ2MDevices : (List Z2MDeviceRegistrationInput -> msg) -> Sub msg


{-| Maps device's ModelId to the correct ZigbeeDevice value
-}
lookupDeviceByModelId : String -> Maybe ZigbeeDevice
lookupDeviceByModelId modelId =
    case modelId of
        "TRADFRI bulb E27 CWS 806lm" ->
            Just IkeaTradfriBulbE27Cws806lm

        "DS01" ->
            Just SonoffDs01

        _ ->
            Nothing
