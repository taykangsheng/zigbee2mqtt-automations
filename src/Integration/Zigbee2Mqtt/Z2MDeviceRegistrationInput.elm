module Integration.Zigbee2Mqtt.Z2MDeviceRegistrationInput exposing (..)


type alias Z2MDeviceRegistrationInput =
    { friendlyName : String
    , modelId : String
    }
