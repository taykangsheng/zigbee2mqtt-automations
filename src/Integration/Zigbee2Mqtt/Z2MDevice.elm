module Integration.Zigbee2Mqtt.Z2MDevice exposing (..)

import Integration.Zigbee2Mqtt.Main exposing (lookupDeviceByModelId)
import Integration.Zigbee2Mqtt.Z2MDeviceRegistrationInput exposing (Z2MDeviceRegistrationInput)
import Json.Encode as E
import MQTT
import ZigbeeDevice exposing (ZigbeeDevice(..))


type alias Z2MDevice =
    { friendlyName : String
    , zigbeeDevice : ZigbeeDevice
    }


{-| Find device based on friendlyName
-}
findRegisteredDeviceFromMqttTopic : String -> List Z2MDevice -> Maybe Z2MDevice
findRegisteredDeviceFromMqttTopic topic registeredDevices =
    registeredDevices
        |> List.filter (\d -> getZ2MTopicName d == topic)
        |> List.head


{-| Find device based on friendlyName
-}
findRegisteredDevice : String -> List Z2MDevice -> Maybe Z2MDevice
findRegisteredDevice friendlyName registeredDevices =
    registeredDevices
        |> List.filter (\d -> d.friendlyName == friendlyName)
        |> List.head


{-| Create MQTT Event for Z2MDevice
-}
createMqttEvent : E.Value -> Z2MDevice -> MQTT.Event
createMqttEvent json z2mDevice =
    { topic = getZ2MSetTopicName z2mDevice
    , message = E.encode 0 json
    }


{-| Find ZigbeeDevice based on model name
-}
lookupDevice : Z2MDeviceRegistrationInput -> Maybe Z2MDevice
lookupDevice input =
    input.modelId
        |> lookupDeviceByModelId
        |> Maybe.map
            (\device ->
                Just { friendlyName = input.friendlyName, zigbeeDevice = device }
            )
        |> Maybe.withDefault Nothing


getZ2MTopicName : Z2MDevice -> String
getZ2MTopicName device =
    "zigbee2mqtt/" ++ device.friendlyName


getZ2MSetTopicName : Z2MDevice -> String
getZ2MSetTopicName device =
    getZ2MTopicName device ++ "/set"
