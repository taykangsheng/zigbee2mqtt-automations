module Config exposing (..)

import Automation exposing (Automation, XAction)
import MQTT
import ZigbeeDevice.SonoffDs01


{-| Not sure where to put these stuff, but these are like config that is meant to be
changed by me to configure the system.
-}
automationRules : MQTT.Event -> List Automation
automationRules mqttEvent =
    [ Automation
        "Main Door Sensor"
        [ ZigbeeDevice.SonoffDs01.getState mqttEvent.message
            |> Result.map (\a -> ZigbeeDevice.SonoffDs01.isOpen a == False)
            |> Result.withDefault False
        ]
        [ XAction "Ikea Bulb" Automation.Off ]
    , Automation
        "Main Door Sensor"
        [ ZigbeeDevice.SonoffDs01.getState mqttEvent.message
            |> Result.map (\a -> ZigbeeDevice.SonoffDs01.isOpen a == True)
            |> Result.withDefault False
        ]
        [ XAction "Ikea Bulb" Automation.On ]
    ]
