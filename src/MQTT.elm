port module MQTT exposing (..)

{-| This library describes a MQTT connection and the interactions with it.


# Connection

@docs Config, State, connectToMQTT, ConnectionResponse, gotMQTTConnection


# Subscribing to topics

@docs subscribeTopic


# Sending and recieving MQTT messages

@docs Event, sendMQTTEvent, gotMQTTEvent


# Common Helpers

@docs eventLog

-}

import Json.Encode as E


{-| Describes the properties of an active MQTT connection.
-}
type alias Config =
    { address : String
    , subscribedTopics : List String
    }


{-| Describes the current state of MQTT connection.

    pendingMqttState : State
    pendingMqttState =
        PendingConnection

    connectedMqttState : Config -> State
    connectedMqttState config =
        Connected config

    errorMqttState : State
    errorMqttState =
        Error "Fail to connect to Mqtt Broker"

-}
type State
    = PendingConnection
    | Connected Config
    | Error String


mapConfig : (Config -> Config) -> Config -> Config
mapConfig mapFn config =
    mapFn config


{-| Initialize connection with MQTT Broker at address.

    commands : Cmd Msg
    commands =
        connectToMQTT "mqtt://192.168.0.141"

**Note**: You will need to manage the persistence of the MQTT connection object outside of the Elm app.

-}
port connectToMQTT : String -> Cmd msg


{-| Describes the response from gotMQTTConnection port.

    ```javascript
    Elm.ports.gotMQTTConnection.send({
        address: "mqtt://127.0.0.1",
        error: "Error connecting to MQTT Broker"
    })
    ```

-}
type alias ConnectionResponse =
    { address : String, error : Maybe String }


{-| Receive MQTT Connection status.

    type Msg
        = GotMQTTConnection ConnectionResponse

    subscriptions : Model -> Sub Msg
    subscriptions _ =
        gotMQTTConnection RegisterMQTTConnection

-}
port gotMQTTConnection : (ConnectionResponse -> msg) -> Sub msg


{-| Subscribe to topic with MQTT Broker.

    subscribeTopic "zigbee2mqtt/Ikea Bulb"

-}
port subscribeTopic : String -> Cmd msg


{-| Receive subscribed topic with MQTT Broker

    type Msg
        = GotSubscribedMQTTTopic String

    subscriptions : Model -> Sub Msg
    subscriptions _ =
        gotSubscribedTopic GotSubscribedMQTTTopic

-}
port gotSubscribedTopic : (String -> msg) -> Sub msg


{-| Describes a MQTT Event to be sent or received.

    event : Event
    event =
        { topic = "zigbee2mqtt/Bulb", message = "Hello, World!" }

-}
type alias Event =
    { topic : String
    , message : String
    }


{-| Send MQTT event.

    sendMQTTEvent
        { topic = "zigbee2mqtt/Ikea Bulb/set"
        , message = E.encode 0 (ZigbeeDevice.IkeaBulb.toggle True)
        }

-}
port sendMQTTEvent : Event -> Cmd msg


{-| Receive MQTT event.

    type Msg
        = GotMQTTEvent Event

    subscriptions : Model -> Sub Message
    subscriptions _ =
        gotMQTTEvent GotMQTTEvent

-}
port gotMQTTEvent : (Event -> msg) -> Sub msg


{-| Helper method to Encode Event into a Json.Value

    eventToJSON { topic = "zigbee2mqtt/Bulb", message = "Hello, World!" }

-}
eventToJSON : Event -> E.Value
eventToJSON event =
    E.object
        [ ( "topic", E.string event.topic )
        , ( "message", E.string event.message )
        ]
