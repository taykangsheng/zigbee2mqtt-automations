module ZigbeeDevice.IkeaTradfriBulbE27Cws806lm exposing (..)

import Json.Encode as E


toggle : Bool -> E.Value
toggle turnOn =
    E.object
        [ ( "state"
          , E.string
                (if turnOn then
                    "ON"

                 else
                    "OFF"
                )
          )
        ]


brightness : Int -> E.Value
brightness i =
    E.object
        [ ( "brightness"
          , E.int
                (i
                    |> max 0
                    |> min 254
                )
          )
        ]
