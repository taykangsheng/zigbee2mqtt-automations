module ZigbeeDevice.SonoffDs01 exposing (..)

import Json.Decode as D


type alias State =
    { contact : Bool
    }


decoder : D.Decoder State
decoder =
    D.map
        State
        (D.field "contact" D.bool)


getState : String -> Result D.Error State
getState mqttEvent =
    D.decodeString decoder mqttEvent


isOpen : State -> Bool
isOpen state =
    not state.contact
