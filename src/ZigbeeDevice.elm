module ZigbeeDevice exposing (..)

{- Should autogenerate this file and ZigbeeDevice/*.elm from https://github.com/Koenkk/zigbee-herdsman-converters/tree/master/src/devices -}


type
    ZigbeeDevice
    {- Custom Type naming convention <PascalCase-Vendor><PascalCase-ModelId> and ModelId can be found in `database.db`

       According to https://www.zigbee2mqtt.io/advanced/support-new-devices/01_support_new_devices.html#_2-1-extending-the-external-definition,
       the `model` that we see in in zigbee2mqtt website (eg. https://www.zigbee2mqtt.io/devices/LED1836G9.html) is supposed
       to be the real model of the device written on the device itself or product page. If that is the case, then there
       is a chance that there might be clashes in model id.

       From my observations after studying `database.db`, it seems like the "model name" itself is the unique identifier
       of devices.
    -}
    = IkeaTradfriBulbE27Cws806lm -- https://www.zigbee2mqtt.io/devices/LED1836G9.html
    | SonoffDs01 -- https://www.zigbee2mqtt.io/devices/SNZB-04.html
