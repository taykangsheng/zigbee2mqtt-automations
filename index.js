/**
 * This is the compiled file that needs to be run for the system to start.
 * This file can be executed simply by running `node index.js`.
 */

const mqtt = require("mqtt")
const { Elm } = require("./bin/automations.js")
const { readFileSync } = require("node:fs")
const util = require("util")
const yaml = require("js-yaml")

let mqttClient = undefined
const AutomationsApp = Elm.Main.init({
  flags: {
    mqttAddress: "mqtt://192.168.0.141"
  }
})

/**
 * When the app is ready, the app will initiate connection with MQTT broker. Once the connection with MQTT broker is
 * established, we will automatically register all devices connected to Zigbee2MQTT with the automation pipeline.
 */
AutomationsApp.ports.connectToMQTT.subscribe(connectToMQTT)

/**
 * When devices are registered with the automation pipeline, we can then subscribe to all relevant MQTT topics needed for
 * the automation pipeline.
 */
AutomationsApp.ports.subscribeTopic.subscribe(subscribeToMqttTopic)

/**
 * When automation pipeline is triggered, we will send event to the specific MQTT topic to control the devices.
*/
AutomationsApp.ports.sendMQTTEvent.subscribe(sendMQTTEvent)

/**
 * Logging utility
 */
AutomationsApp.ports.log.subscribe((content) => logger(content))




function connectToMQTT(mqttAddr) {
  mqttClient = mqtt.connect(mqttAddr)

  // When there is an error connecting to the MQTT broker, update the MQTT state in the App.
  mqttClient.on("error", (error) =>
    AutomationsApp.ports.gotMQTTConnection.send({ address: mqttAddr, error: JSON.stringify(error) })
  )

  // When connection with MQTT Broker is established, update the MQTT state in the App and register all connected
  // zigbee2mqtt devices to the App.
  mqttClient.on("connect", () => {
    AutomationsApp.ports.gotMQTTConnection.send({ address: mqttAddr, error: null })
    AutomationsApp.ports.registerZ2MDevices.send(discoverDevices())
  })

  // When received any MQTT messages from subscribed topics, send it to the App for processing.
  mqttClient.on("message", (topic, message, packet) => {
    AutomationsApp.ports.gotMQTTEvent.send({ topic: topic, message: message.toString() })
  })
}

function subscribeToMqttTopic(topic) {
  if (!mqttClient) return

  mqttClient.subscribe(topic, (err) => {
    if (err) throw new Error(`failed to subscribe to ${topic}`)

    AutomationsApp.ports.gotSubscribedTopic.send(topic)
  })
}

function sendMQTTEvent({ topic, message }) {
  if (!mqttClient) return

  mqttClient.publish(topic, message)
}

function discoverDevices() {
  // Read configuration.yaml and retrieve all the friendly_names of connected devices.
  const deviceConfigurationPath = "./zigbee2mqtt-data/configuration.yaml"
  const deviceConfigurationContent = yaml.load(readFileSync(deviceConfigurationPath, "utf8"));
  const deviceConfigs = deviceConfigurationContent.devices

  // Read database.db and retrieve all the registered devices.
  const databasePath = "./zigbee2mqtt-data/database.db"
  const databaseContent = readFileSync(databasePath)
    .toString()
    .split("\n")
    .filter((data) => data !== "")
    .map((data) => JSON.parse(data))
  const devices = databaseContent
    // Filter out coordinator devices.
    .filter((data) => data.type !== "Coordinator")

  // Combine device information with friendlyName.
  const devicesWithFriendlyName = devices
    .map((data) => {
      const deviceConfig = deviceConfigs[data["ieeeAddr"]]
      return deviceConfig ? { ...data, friendlyName: deviceConfig['friendly_name'] } : null
    })
    .filter((device) => device !== null)


  // Note: There is a possibility of loading the current state of each device via state.json provided by zigbee2mqtt.
  return devicesWithFriendlyName
}

/**
 * Note: Maybe provide other ways to logs instead of just sending to console.
 */
function logger(object) {
  console.log(
    util.inspect(
      { ...object, timestamp: Date.now() },
      { showHidden: false, depth: null, colors: true }
    )
  )
}
